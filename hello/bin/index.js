#!/usr/bin/env node

const axios = require("axios");
const boxen = require("boxen");
const chalk = require("chalk");
const yargs = require("yargs");

const options = yargs
      .usage("Usage: -n <name>")
      .option("n", {alias: "name", describe: "Your name", type: "string", demandOption: true})
      .option("s", {alias: "search", describe: "Search term", type: "string"})
      .argv;

const boxenOptions = {
  padding: 1,
  margin: 1,
  borderStyle: "round",
  borderColor: "green",
  backgroundColor: "#555555"
};

const greeting = chalk.white.bold(`Hello, ${options.name}!`);
const msgBox = boxen(greeting, boxenOptions);
console.log(msgBox);

if (options.search) {
  console.log(`Searching for dad jokes about ${options.search}...`)
} else {
  console.log("Here's a random dad joke:")
}

const jokeUrl = options.search ?  `https://icanhazdadjoke.com/search?term=${escape(options.search)}` : "https://icanhazdadjoke.com/";


axios.get(jokeUrl, {headers: {Accept: "application/json"}})
  .then(res => {
    if (options.search) {
      res.data.results.forEach(j => {
	console.log("\n" + j.joke)
      });
      if (res.data.results.length === 0) {
	console.log("No jokes found 😱")
      }
    } else {
      jokeText = chalk.white.bold(res.data.joke);
      jokeBox = boxen(jokeText, boxenOptions);
      console.log(jokeBox);
    }
  });
