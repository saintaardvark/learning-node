#!/usr/bin/env node

const axios = require("axios");
const table = require("table");

console.log("Hello, world!  Let's make a random API request.");

const apiUrl = "https://api.publicapis.org/entries?auth=apikey";

axios.get(apiUrl, {headers: {Accept: "application/json"}})
  .then(res => {
    apis = res.data;
    tableData = [];
    for (const [key, value] of Object.entries(apis['entries'])) {
      apiTitle = value.API;
      apiDesc = value.Description;
      apiLink = value.Link;
      tableData.push([apiTitle, apiDesc, apiLink]);
    }
    var item = tableData[Math.floor(Math.random() * tableData.length)];
    output = table.table([item]);
    console.log(output);
  })
  .catch((error) => console.log(error));
